# eap-6.3_install.yml
Ansible playbooks for installing JBoss EAP 6.3.
 
# Getting Started

* Create the Directory structures required for JBoss EAP 6.3 installation.
* Create the soft link in linux for complying the JBoss EAP 6.3 specific directory structure.
* Installs & configures JBoss EAP 6.3.
 
## Running Playbook
ansible-playbooks -i inventories/hosts -e __host_group_all=jboss_eap eap-6.3_install.yml

### Parameters to be modified
* run_as - User to run the playbook
* cwp_initd_script_checksum: 
* cwp_jboss_eap_checksum: Checksum for the JBoss EAP 6.3 binaries
* cwp_java_checksum: Checksum for the Java JDK 1.8 binaries
* eap_repo_username: User name for JBoss EAP repo
* eap_repo_password: Password for JBoss EAP repo

#### Chatbot uninstallation

Run the following playbook to uninstall datarobot

ansible-playbook -i inventories -e __host_group_all=jboss_eap eap-6.3_uninstall.yml 