# jboss_datasource_maintenance.yml
Ansible playbooks for installing JBoss EAP 6.3.
 
# Getting Started

* Add a datasource in JBoss 6.3.
* Remove a datasource in JBoss 6.3.
* Update the a Username & Password of a datasource in JBoss 6.3.
 
## Running Playbook
ansible-playbook -i inventories/hosts jboss_datasource_maintenance_final.yml -e operation= (add / remove / update)

### Parameters to be modified
All the required parameters needs to be modified to create a new datasource.
* mssql_datasource_name: java\:jboss\/sqlserver\/major
* mssql_connection: jdbc:sqlserver://10.174.62.188:1433; DatabaseName=PLT_CWP_CRUD
* mssql_username: temps
* mssql_password: Pass1234
* mssql_driver_name: sqljdbc4.jar
* mssql_valid_connection_value: "SELECT 1"
* mssql_background_validation_value: true
* mssql_jndi_name: java\:jboss\/sqlserver
* mssql_background_validation_millis_value: 60000
* mssql_min_pool_size: 10
* mssql_max_pool_size: 50
* mssql_pool_prefill_value: false
* mssql_driver_module_name: com.microsoft
* mssql_driver_xa_datasource_class_name: com.microsoft.sqlserver.jdbc.SQLServerXADataSource