This playbook performs the following tasks:

    [+] Copy postgres and ms-sql jdbc dirvers to EAP server
    [+] Add driver and datasource details in standalone.xml

Instructions to User: 

Running Playbook()
ansible-playbook -i inventories/hosts -e host_group=eaps-dp-jdbc-install.yml.yml

Parameters which can be modified:
run_as - User to run the playbook
postgres_connection - Connection string for postgresql
postgres_username - Username for postgresql db
postgres_password - Password for postgresql db
mssql_connection - Connection string for Azure SQL
mssql_username - Username for Azure SQL
mssql_password - Password for Azure SQL
